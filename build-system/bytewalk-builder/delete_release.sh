#!/bin/bash

bytewalk_project_id="12907289"
read -p "gitlab token: " gitlab_token
read -p "tag name: " tag_name

curl --request DELETE --header "PRIVATE-TOKEN: $gitlab_token" "https://gitlab.com/api/v4/projects/$bytewalk_project_id/releases/$tag_name"
