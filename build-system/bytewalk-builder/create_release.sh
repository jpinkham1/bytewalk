#!/bin/bash

bytewalk_project_id="12907289"
bytewalk_version=$(cat /builds/bytewalk/version)
tag_name="v$bytewalk_version"
release_name="v $bytewalk_version"
release_description="bytewalk v$bytewalk_version release"

read -p "gitlab token: " gitlab_token

# upload artifacts
artifacts=()
for f in $(ls /artifacts/)
do
	filename="/artifacts/$f"
	echo $filename
	resp=$(curl --request POST --header "PRIVATE-TOKEN: $gitlab_token" https://gitlab.com/api/v4/projects/$bytewalk_project_id/uploads --form "file=@$filename")
	url=$(python3 -c "import json;resp='$resp';print(json.loads(resp)['url'])")
	artifact="$f#$url"
	artifacts+=($artifact)
done

links=""
for i in "${artifacts[@]}"
do
	name=$(echo $i | awk -F# '{print $1}')
	url=$(echo $i | awk -F# '{print $2}')
    links="$links{\"name\":\"$name\",\"url\":\"https://gitlab.com/bytesweep/bytewalk$url\"},"
done
links_final=${links::-1}
echo $links_final

# create release
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $gitlab_token" \
     --data "{ \"name\": \"$release_name\", \"tag_name\": \"$tag_name\", \"description\": \"$release_description\", \"assets\": { \"links\": [$links_final] } }" \
     --request POST https://gitlab.com/api/v4/projects/$bytewalk_project_id/releases
