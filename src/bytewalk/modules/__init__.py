# Don't load the disasm module if the capstone module can't be found
try:
    from bytewalk.modules.disasm import Disasm
except ImportError:
    pass

# Don't load the compression module if the lzma module can't be found
try:
    from bytewalk.modules.compression import RawCompression
except ImportError:
    pass

from bytewalk.modules.signature import Signature
from bytewalk.modules.hexdiff import HexDiff
from bytewalk.modules.general import General
from bytewalk.modules.extractor import Extractor
from bytewalk.modules.entropy import Entropy
