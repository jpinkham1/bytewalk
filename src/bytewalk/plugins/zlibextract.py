import os
import zlib
import bytewalk.core.compat
import bytewalk.core.common
import bytewalk.core.plugin


class ZLIBExtractPlugin(bytewalk.core.plugin.Plugin):

    '''
    Zlib extractor plugin.
    '''
    MODULES = ['Signature']

    def init(self):
        # If the extractor is enabled for the module we're currently loaded
        # into, then register self.extractor as a zlib extraction rule.
        if self.module.extractor.enabled:
            self.module.extractor.add_rule(txtrule=None,
                                           regex="^zlib compressed data",
                                           extension="zlib",
                                           cmd=self.extractor)

    def extractor(self, fname):
        outfile = os.path.splitext(fname)[0]

        try:
            fpin = bytewalk.core.common.BlockFile(fname)
            fpout = bytewalk.core.common.BlockFile(outfile, 'w')

            plaintext = zlib.decompress(bytewalk.core.compat.str2bytes(fpin.read()))
            fpout.write(plaintext)

            fpin.close()
            fpout.close()
        except KeyboardInterrupt as e:
            raise e
        except Exception as e:
            return False

        return True
