# This file has been depreciated and is no longer in use.
# This is merely a placeholder to ensure future installations
# don't leave the old, depreciated file in tact.

import bytewalk.core.plugin


class Unjffs2DepreciatedPlugin(bytewalk.core.plugin.Plugin):
    pass
