#!/usr/bin/env python

import bytewalk

# Since no options are specified, they are by default taken from sys.argv.
# Effecitvely, this duplicates the functionality of the normal bytewalk script.
bytewalk.scan()
